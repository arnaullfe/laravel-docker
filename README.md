START PROJECT:


    1- 
        Exec -> "docker-compose up"

    2- 
        Find the php container and exec:
        "docker-compose exec php php artisan config:cache"

    3-
        EXEC -> "chmod -R 777 ./storage"

    4-
        EXEC -> "docker-compose run composer composer require "laravel/ui""

    5-
        EXEC -> "docker-compose exec php php artisan ui vue --auth"

    6-
        Find the node.js container and exec:
        "docker-compose exec node npm install"
        "docker-compose exec node npm run dev"
        "docker-compose exec node npm run watch"
        "docker-compose exec php php artisan migrate"


EXPLICATION:


    In the docker file there are some containers and services. We can found a service called "composer", and with this service 
    we can execute all the commands of composer.

    For execute php artisan commands, we can use the php container.

    For npm commands we can use node.js container
